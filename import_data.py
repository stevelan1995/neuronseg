import numpy

import numpy as np

import matplotlib.pyplot as plt

train_img = np.load("./brain_electron_microscopy/train_images.npy")

train_labels = np.load('./brain_electron_microscopy/train_labels.npy')

nb_img_to_print = 2

for img, label, i in izip(train_img, train_labels, range(nb_img_to_print)):
    plt.imshow(img)
    plt.savefig("./img_%s.png"%i)
    plt.imshow(label)
    plt.savefig("./label_%s.png"%i)