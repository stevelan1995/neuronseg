from scipy import stats
"""
this algorithm serves the purpose of clean noisy spots in the result
"""
detect_bound = 2

for i, row in enurerate(pixels):
	for j, col in pixels:
		
		detect_range = pixel_output[i-detect_bound:i+detect_bound, j-detect_bound:j+detect_bound]
		range_mode = stats.mode(detect_range)
		if range_mode[2][0] > (detect_bound*2)^2 and range_mode[1][0] is not pixel[i,j]:
			pixel[i,j] = range_mode[1][0]

