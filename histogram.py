
import os.path as path
import numpy as np
import matplotlib.pyplot as plt

np.set_printoptions(threshold=np.nan)

train_img = np.load(path.expanduser("~/Desktop/datathon/brain_electron_microscopy/train_images.npy"))
# train_labels = np.load(path.expanduser("~/Desktop/datathon/brain_electron_microscopy/train_labels.npy"))

nb_img_to_print = 2


img = train_img[1]
# print(img)
img_1 = img[25:75,25:75]
# print (img_1)
# non_boundary_index = np.where(img_1 <= 160)[0]

# img_1[:,np.where(img_1 <= 80)[0]] = 1

print(img_1)

plt.imshow(img_1)
plt.show()
# i = 1
# plt.savefig("./img_%s.png"%i)

# imghist = img_1.flatten()
# print (max(imghist))

# for img, label, i in izip(train_img, train_labels, range(nb_img_to_print)):
#     plt.imshow(img)
#     plt.savefig("./img_%s.png"%i)
#     plt.imshow(label)
#     plt.savefig("./label_%s.png"%i)

print("size train",img_1.shape)

hist_data, bin_edges = np.histogram(img_1.flatten(),density=True)
# n, bins, patches = plt.hist(img_1.flatten(), density=True, facecolor='green')

# l = plt.plot(hist_data)

# plt.xlabel('Smarts')
# plt.ylabel('Probability')
# plt.axis([40, 160, 0, 0.03])
# plt.grid(True)

# figure2 = plt.figure()
# f2 = figure2.add_subplot(222)
# plt.hist((bin_edges,hist_data),bins=bin_edges)
# plt.show()

# figure2.savefig("./imghis_%s.png"%i)
# print("size label",train_labels.shape)
