import os.path as path
import numpy as np
import matplotlib.pyplot as plt

np.set_printoptions(threshold=np.nan)

train_img = np.load(path.expanduser("./train_images.npy"))
# train_labels = np.load(path.expanduser("~/Desktop/datathon/brain_electron_microscopy/train_labels.npy"))

nb_img_to_print = 2



for k in range(0,len(train_img)):
	print (k)
	img = train_img[k]

	for i, row in enumerate(img):
		for j, pixel in enumerate(row):
			if 70<= img[i][j] <= 110:
				img[i][j] = 255
			elif img[i][j] <= 180:
				img[i][j] = 0

	plt.imshow(img)
	plt.savefig("./boundary/img_%s.png"%k)