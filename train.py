import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

import numpy as np
import os.path as path
from keras import optimizers
from keras.utils.np_utils import to_categorical
from models import MR_conv_dial, MR_conv_kern, Resnet


candidate_model = {
    'MR_conv_dial': MR_conv_dial, 
    'MR_conv_kern': MR_conv_kern,
    'Resnet': Resnet
}

import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.9
set_session(tf.Session(config=config))


def prepare_callbacks(model_folder, test_name, input_shape, tr_params, test_params, use_adaptive_optimzer=True, validate_model=False, val_data=None):
    reduce_lr = ReduceLROnPlateau(monitor='loss', factor=0.2, patience=5, min_lr=0.000001, verbose=1, mode="min")
    
    model_name = "weights_{epoch:05d}.hdf5"
    saved_model_path = model_folder+'/'+model_name
    csv_log_name = "training_log.csv"

    save_chkpt = ModelCheckpoint(
        saved_model_path,
        verbose=1,
        save_best_only=False,
        monitor='loss',
        mode='auto',
        period=tr_params["save_per_step"]
    )


    if use_adaptive_optimzer:
        callback_list = [save_chkpt, tr_logger]
    else:   
        callback_list = [reduce_lr, save_chkpt, tr_logger]
    
    return callback_list


def data_generator(x_list, y_list, model_params, **kwargs):

    for i, x, y in zip(range(375), x_list, y_list):
        # pad x by 2^causal_layer
        pad_size = int(2**model_params['causal_layer']/2)
        # x = np.pad(x, [(pad_size, pad_size), (pad_size, pad_size)], mode='constant')
        x = x.reshape(1, x.shape[0], x.shape[1],1)
        print("X shape {}".format(x.shape))

        class_y = to_categorical(y[0], model_params['output_classes'])[np.newaxis]

        yield x, class_y


def run_training(x, y, model_name, model_params, tr_params): 

    model = candidate_model[model_name](**model_params).model

    model.summary()

    model.compile(
        loss='categorical_crossentropy',
        optimizer='adam', 
        metrics=['categorical_accuracy']
    )

    # Preprocess data


    # for x, y in data_generator(x, y, model_params):
    #     print("X: {}\n shape {}".format(x, x.shape))
    #     print("Y: {}".format(y))

    # start training
    model.fit_generator(
        generator = data_generator(x, y, model_params),
        steps_per_epoch = tr_params["steps_per_epoch"], 
        # validation_steps = training_params["sample_per_period"],
        epochs = tr_params['epochs'],
        max_queue_size = 1,
        verbose=1
    )


def main():
    # Training
    # model paths

    # model_folder = path.expanduser("~/Desktop/datathon/models/")
    # data_folder = path.expanduser("~/Desktop/datathon/data/")
    model_folder = path.expanduser("./model_file/")
    data_folder = path.expanduser("../brain_data/")

    train_set = np.load(data_folder+'train_images/0.npy')
    train_labels = np.load(data_folder+'train_labels/0.npy')

    # test_set = np.load(data_folder+'test_images.npy')

    # preprocess y such that all values in y are unique
    # argsort unique y and save argsorted y
    # use index as real class value and map it to one hot
    # when testing, use real value output as index 
    # and convert it back to real pixel value from unique_ys

    print("Processing label data... Please wait.")
    unique_ys = np.unique(train_labels.flatten())
    # y_class_values = np.argsort(unique_ys)
    y_class_values = [(i,y) for i,y in enumerate(unique_ys)]

    print("Process finished. Start training.")

    model_name = 'Resnet'

    model_params = {
        'input_shape': (train_set.shape[1]+int(2**1), train_set.shape[2]+int(2**1),1),
        # 'input_shape': (64, 64, 1),

        'nb_filters': 4,
        'output_classes': 40591,
        # 'output_classes': 10,
        'dilation_depth': 1,
        'filter_width': 3,
        'nb_resblocks': 1,
        'padding': 'same',
        'nb_stacks': 1,
        'use_skip_connections': True,
        'learn_all_outputs': True,
        'use_bias': True,
        'causal_layer':1
    }   

    tr_params = {
        "steps_per_epoch": 1,
        "epochs": train_set.shape[0]
    }

    print(train_set.shape)
    # train_set = np.random.randint(0,255, size=(train_set.shape[0], 64, 64, 1))
    run_training(train_set, y_class_values, model_name, model_params, tr_params)



if __name__ == '__main__':
    main()


