
"""
two possibilities to do classification:
1. classify using dilated block
2. classify use kernel block
"""
from keras.models import Model
from keras.layers.convolutional import Conv2D
from keras.layers.core import Activation, Flatten, Dense, Lambda
from keras.layers import add, multiply, pooling, ZeroPadding2D, MaxPooling2D, AveragePooling2D, Add
from keras.layers import merge, Input, BatchNormalization
from keras.constraints import max_norm
from keras.initializers import glorot_uniform

class Resnet(object):
    def __init__(self, **params):
        """
        Resnet model.

            Args:
                input_shape (tuple of integers):
                    input dimension

                nb_filters (int):
                    how many filters in each layer
                
                output_classes (int):
                    the depth of output

                dilation_depth (int):
                
                nb_resblocks (int):
                    Number of resnet blocks
                
                use_bias (bool):
        """
        model_params = {
            'input_shape': params['input_shape'],
            'filters': params['nb_filters'],
            'nb_classes': params['output_classes'],
            'filter_width': params['filter_width'],
            'nb_resblocks': params['nb_resblocks'],
            'padding': params['padding']
        }
        
        x, y = self.build_resnet_original(**model_params)
        self.model = Model(inputs=x, outputs=y)
        # self.model = self.ResNet50(model_params['input_shape'], classes=model_params['nb_classes'])

    def identity_block(self, X, f, filters, stage, block):
        """
        Implementation of the identity block as defined in Figure 3
        
        Arguments:
        X -- input tensor of shape (m, n_H_prev, n_W_prev, n_C_prev)
        f -- integer, specifying the shape of the middle CONV's window for the main path
        filters -- python list of integers, defining the number of filters in the CONV layers of the main path
        stage -- integer, used to name the layers, depending on their position in the network
        block -- string/character, used to name the layers, depending on their position in the network
        
        Returns:
        X -- output of the identity block, tensor of shape (n_H, n_W, n_C)
        """
        
        # defining name basis
        conv_name_base = 'res' + str(stage) + block + '_branch'
        bn_name_base = 'bn' + str(stage) + block + '_branch'
        
        # Retrieve Filters
        F1, F2, F3 = filters
        
        # Save the input value. You'll need this later to add back to the main path. 
        X_shortcut = X
        
        # First component of main path
        X = Conv2D(filters = F1, kernel_size = (1, 1), strides = (1,1), padding = 'valid', name = conv_name_base + '2a', kernel_initializer = glorot_uniform(seed=0))(X)
        X = BatchNormalization(axis = 3, name = bn_name_base + '2a')(X)
        X = Activation('relu')(X)
        
        ### START CODE HERE ###
        
        # Second component of main path (≈3 lines)
        X = Conv2D(filters=F2, kernel_size=(f,f), strides=(1,1), padding='same', name=conv_name_base+'2b', kernel_initializer=glorot_uniform(seed=0))(X)
        X = BatchNormalization(axis = 3, name = bn_name_base + '2b')(X)
        X = Activation('relu')(X)

        # Third component of main path (≈2 lines)
        X = Conv2D(filters=F3, kernel_size=(1,1), strides=(1,1), padding='valid', name=conv_name_base+'2c', kernel_initializer=glorot_uniform(seed=0))(X)
        X = BatchNormalization(axis = 3, name = bn_name_base + '2c')(X)

        # Final step: Add shortcut value to main path, and pass it through a RELU activation (≈2 lines)
        X = Add()([X, X_shortcut])
        X = Activation('relu')(X)
        
        ### END CODE HERE ###
        
        return X

    def convolutional_block(self, X, f, filters, stage, block, s = 2):
        """
        Implementation of the convolutional block as defined in Figure 4
        
        Arguments:
        X -- input tensor of shape (m, n_H_prev, n_W_prev, n_C_prev)
        f -- integer, specifying the shape of the middle CONV's window for the main path
        filters -- python list of integers, defining the number of filters in the CONV layers of the main path
        stage -- integer, used to name the layers, depending on their position in the network
        block -- string/character, used to name the layers, depending on their position in the network
        s -- Integer, specifying the stride to be used
        
        Returns:
        X -- output of the convolutional block, tensor of shape (n_H, n_W, n_C)
        """
        
        # defining name basis
        conv_name_base = 'res' + str(stage) + block + '_branch'
        bn_name_base = 'bn' + str(stage) + block + '_branch'
        
        # Retrieve Filters
        F1, F2, F3 = filters
        
        # Save the input value
        X_shortcut = X


        ##### MAIN PATH #####
        # First component of main path 
        # The first CONV2D has  F1  filters of shape (1,1) and a stride of (s,s). Its padding is "valid" and its name should be conv_name_base + '2a'.
        X = Conv2D(filters=F1, kernel_size=(1, 1), strides = (s,s), name = conv_name_base + '2a', padding='valid', kernel_initializer = glorot_uniform(seed=0))(X)
        X = BatchNormalization(axis = 3, name = bn_name_base + '2a')(X)
        X = Activation('relu')(X)
        
        ### START CODE HERE ###

        # Second component of main path (≈3 lines)
        # The second CONV2D has  F2  filters of (f,f) and a stride of (1,1). Its padding is "same" and it's name should be conv_name_base + '2b'.
        X = Conv2D(filters=F2, kernel_size=(f, f), strides = (1,1), name = conv_name_base + '2b', padding='same', kernel_initializer = glorot_uniform(seed=0))(X)
        X = BatchNormalization(axis = 3, name = bn_name_base + '2b')(X)
        X = Activation('relu')(X)

        # Third component of main path (≈2 lines)
        # The third CONV2D has  F3  filters of (1,1) and a stride of (1,1). Its padding is "valid" and it's name should be conv_name_base + '2c'.
        X = Conv2D(filters=F3, kernel_size=(1, 1), strides = (1,1), name = conv_name_base + '2c', padding='valid', kernel_initializer = glorot_uniform(seed=0))(X)
        X = BatchNormalization(axis = 3, name = bn_name_base + '2c')(X)
        
        ##### SHORTCUT PATH #### (≈2 lines)
        # The CONV2D has  F3F3  filters of shape (1,1) and a stride of (s,s). Its padding is "valid" and its name should be conv_name_base + '1'.
        X_shortcut = Conv2D(filters=F3, kernel_size=(1, 1), strides = (s,s), name = conv_name_base + '1', padding='valid', kernel_initializer = glorot_uniform(seed=0))(X_shortcut)
        X_shortcut = BatchNormalization(axis = 3, name = bn_name_base + '1')(X_shortcut)

        # Final step: Add shortcut value to main path, and pass it through a RELU activation (≈2 lines)
        X = Add()([X, X_shortcut])
        X = Activation('relu')(X)
        
        ### END CODE HERE ###
        
        return X

    def ResNet50(self, input_shape = (64, 64, 3), classes = 6):
        """
        Implementation of the popular ResNet50 the following architecture:
        CONV2D -> BATCHNORM -> RELU -> MAXPOOL -> CONVBLOCK -> IDBLOCK*2 -> CONVBLOCK -> IDBLOCK*3
        -> CONVBLOCK -> IDBLOCK*5 -> CONVBLOCK -> IDBLOCK*2 -> AVGPOOL -> TOPLAYER

        Arguments:
        input_shape -- shape of the images of the dataset
        classes -- integer, number of classes

        Returns:
        model -- a Model() instance in Keras
        """
        
        # Define the input as a tensor with shape input_shape
        X_input = Input(input_shape)

        
        # Zero-Padding
        X = ZeroPadding2D((3, 3))(X_input)
        
        # Stage 1
        X = Conv2D(64, (7, 7), strides = (2, 2), name = 'conv1', kernel_initializer = glorot_uniform(seed=0))(X)
        X = BatchNormalization(axis = 3, name = 'bn_conv1')(X)
        X = Activation('relu')(X)
        X = MaxPooling2D((3, 3), strides=(2, 2))(X)

        # Stage 2
        X = self.convolutional_block(X, f = 3, filters = [4,4,16], stage = 2, block='a', s = 1)
        X = self.identity_block(X, 3, [4,4,16], stage=2, block='b')
        X = self.identity_block(X, 3, [4,4,16], stage=2, block='c')

        ### START CODE HERE ###

        """
        The convolutional block uses three set of filters of size [32,32,64], "f" is 3, "s" is 2 and the block is "a".
        The 3 identity blocks use three set of filters of size [32,32,64], "f" is 3 and the blocks are "b", "c" and "d".
        """

        
        # Stage 3 (≈4 lines)
        # X = self.convolutional_block(X, 3, [32,32,64], 3, 'a', s = 2)
        # X = self.identity_block(X, 3, [32,32,64], 3, 'b')
        # X = self.identity_block(X, 3, [32,32,64], 3, 'c')
        # X = self.identity_block(X, 3, [32,32,64], 3, 'd')
        
        """
        The convolutional block uses three set of filters of size [256, 256, 1024], "f" is 3, "s" is 2 and the block is "a".
        The 5 identity blocks use three set of filters of size [256, 256, 1024], "f" is 3 and the blocks are "b", "c", "d", "e" and "f".
        """
        # convolutional_block(X, f, filters, stage, block, s = 2)
        # identity_block(X, f, filters, stage, block)
        
        # Stage 4 (≈6 lines)
        # X = self.convolutional_block(X, 3, [256, 256, 1024], stage=4, block='a', s = 2)
        # X = self.identity_block(X, 3, [256, 256, 1024], stage=4, block='b')
        # X = self.identity_block(X, 3, [256, 256, 1024], stage=4, block='c')
        # X = self.identity_block(X, 3, [256, 256, 1024], stage=4, block='d')
        # X = self.identity_block(X, 3, [256, 256, 1024], stage=4, block='e')
        # X = self.identity_block(X, 3, [256, 256, 1024], stage=4, block='f')

        # """
        # The convolutional block uses three set of filters of size [512, 512, 2048], "f" is 3, "s" is 2 and the block is "a".
        # The 2 identity blocks use three set of filters of size [512, 512, 2048], "f" is 3 and the blocks are "b" and "c".
        # """
        # # Stage 5 (≈3 lines)
        # X = self.convolutional_block(X, 3, [512, 512, 2048], stage=5, block='a', s = 2)
        # X = self.identity_block(X, 3, [512, 512, 2048], stage=5, block='b')
        # X = self.identity_block(X, 3, [512, 512, 2048], stage=5, block='c')

        """
        The 2D Average Pooling uses a window of shape (2,2) and its name is "avg_pool".
        The flatten doesn't have any hyperparameters or name.
        The Fully Connected (Dense) layer reduces its input to the number of classes using a softmax activation. Its name should be 'fc' + str(classes).
        """
        # AVGPOOL (≈1 line). Use "X = AveragePooling2D(...)(X)"
        X = AveragePooling2D((2,2), name='avg_pool')(X)
        
        ### END CODE HERE ###

        # output layer
        X = Flatten()(X)
        X = Dense(classes, activation='softmax', name='fc' + str(classes), kernel_initializer = glorot_uniform(seed=0))(X)
        
        
        # Create model
        model = Model(inputs = X_input, outputs = X, name='ResNet50')

        return model

    def build_resnet_original(self,
                              input_shape, 
                              filters, 
                              nb_classes, 
                              filter_width,
                              padding,
                              nb_resblocks=3):
        print("Building ResNet")
        print("input shape", input_shape)
        x = Input(shape=(input_shape))
        # conv_x = BatchNormalization()(x)
        conv_x = Conv2D(filters, filter_width, padding=padding)(x)
        conv_x = BatchNormalization()(conv_x)
        conv_x = Activation('relu')(conv_x)
         
        # print ('build conv_y')
        conv_y = Conv2D(filters, filter_width, padding=padding)(conv_x)
        conv_y = BatchNormalization()(conv_y)
        conv_y = Activation('relu')(conv_y)
         
        print ('build conv_z')
        conv_z = Conv2D(filters, filter_width, padding=padding)(conv_y)
        conv_z = BatchNormalization()(conv_z)
     
        # Identity block
        channel_expanded = not (input_shape[0] == filters)
        if channel_expanded:
            shortcut_y = Conv2D(filters, filter_width,padding=padding)(x)
            shortcut_y = BatchNormalization()(shortcut_y)
        else:
            shortcut_y = BatchNormalization()(x)
        
        # print ('Merging skip connection')
        y = add([shortcut_y, conv_y])
        y = Activation('relu')(y)
         
        # print ('build conv_x')
        # x1 = y
        # conv_x = Conv2D(filters*2, filter_width, padding=padding)(x1)
        # conv_x = BatchNormalization()(conv_x)
        # conv_x = Activation('relu')(conv_x)
         
        # print ('build conv_y')
        # conv_y = Conv2D(filters*2, filter_width, padding=padding)(conv_x)
        # conv_y = BatchNormalization()(conv_y)
        # conv_y = Activation('relu')(conv_y)
         
        # print ('build conv_z')
        # # conv_z = Conv2D(filters*2, filter_width[3], padding=padding)(conv_y)
        # # conv_z = BatchNormalization()(conv_z)

        # channel_expanded = not (input_shape[-1] == filters)
        # if channel_expanded:
        #     shortcut_y = Conv2D(filters*2, filter_width,padding=padding)(x1)
        #     shortcut_y = BatchNormalization()(shortcut_y)
        # else:
        #     shortcut_y = BatchNormalization()(x1)
        # print ('Merging skip connection')
        # y = add([shortcut_y, conv_y])
        # y = Activation('relu')(y)
         
        # print ('build conv_x')
        # x1 = y
        # conv_x = Conv2D(filters*2, filter_width, padding=padding)(x1)
        # conv_x = BatchNormalization()(conv_x)
        # conv_x = Activation('relu')(conv_x)
         
        # print ('build conv_y')
        # conv_y = Conv2D(filters*2, filter_width, padding=padding)(conv_x)
        # conv_y = BatchNormalization()(conv_y)
        # conv_y = Activation('relu')(conv_y)
         
        # print ('build conv_z')
        # # conv_z = Conv2D(filters*2, filter_width[3], padding=padding)(conv_y)
        # # conv_z = BatchNormalization()(conv_z)

        # channel_expanded = not (input_shape[-1] == filters)
        # if channel_expanded:
        #     shortcut_y = Conv2D(filters*2, filter_width,padding=padding)(x1)
        #     shortcut_y = BatchNormalization()(shortcut_y)
        # else:
        #     shortcut_y = BatchNormalization()(x1)
        # print ('Merging skip connection')
        # y = add([shortcut_y, conv_y])
        # y = Activation('relu')(y)
        
        ############### just like the simplied three conv version
        y = Conv2D(1,1,
            kernel_initializer='random_uniform',
            padding=padding)(y)
            
        y = BatchNormalization()(y)
        y = Flatten()(y)

        y = Dense(64, 
                activation= 'linear',
                kernel_initializer= 'random_uniform',
                use_bias= True,
                bias_initializer= 'glorot_normal')(y)
        ################### just like the stack three layer version

        # full = pooling.GlobalAveragePooling1D()(y)   
        # full = pooling.GlobalMaxPooling1D()(y)
        # full = Dense(nb_classes, activation='linear')(y)
        out = Dense(nb_classes, activation='softmax')(y)

        return x, out





    def build_resnet(self, input_shape, filters, nb_classes, model_params, nb_resblocks=3):
        print("Building ResNet")

        x = Input(shape=(input_shape))
        output_from_last_block = x

        conv_x = BatchNormalization()(output_from_last_block)

        for block_building_iteration in xrange(nb_resblocks):
            # Residual Block
            print("Building resnet block "+str(block_building_iteration+1))
            # print ('build conv_x')
            conv_x = Conv2D(filters, filter_width[0], padding=padding)(conv_x)
            conv_x = BatchNormalization()(conv_x)
            conv_x = Activation('relu')(conv_x)
             
            # print ('build conv_y')
            conv_y = Conv2D(filters, filter_width[0], padding=padding)(conv_x)
            conv_y = BatchNormalization()(conv_y)
            conv_y = Activation('relu')(conv_y)
             
            # print ('build conv_z')
            conv_z = Conv2D(filters, filter_width[0], padding=padding)(conv_y)
            conv_z = BatchNormalization()(conv_z)
         
            # Identity block
            channel_expanded = not (input_shape[-1] == filters)
            if channel_expanded:
                shortcut_y = Conv2D(filters, filter_width[0],padding=padding)(output_from_last_block)
                shortcut_y = BatchNormalization()(shortcut_y)
            else:
                shortcut_y = BatchNormalization()(output_from_last_block)
            
            # print ('Merging skip connection')
            y = add([shortcut_y, conv_z])
            y = Activation('relu')(y)
            
            # Prepare identity mapping for the next layer
            output_from_last_block = y
         
        full = pooling.GlobalAveragePooling1D()(y)   
        # full = Reshape((1,200))(full)
        out = Dense(nb_classes, activation='softmax')(full)

        return x, out


class MR_conv_dial(object):

    def __init__(self, **params):
        """
        MR_conv model.

            Args:
                input_shape (tuple of integers):
                    input dimension (length, height, depth = 1)
                nb_filters (int):
                    how many filters in each layer              
                output_classes (int):
                    the depth of output
                dilation_depth (int):               
                nb_stacks (int):                
                use_skip_connections (bool):                
                learn_all_outputs (bool):           
                use_bias (bool):
        """

        model_params = {
            # 'input_shape': (params['range'],params['input_depth']),
            'input_shape': params['input_shape'],
            'nb_filters': params['nb_filters'],
            'output_classes': params['output_classes'],
            'dilation_depth': params['dilation_depth'],
            'nb_stacks': params['nb_stacks'],
            'use_skip_connections': True,
            'learn_all_outputs': True,
            'use_bias': True
        }

        x, y = self.build_conv(**model_params)
        
        self.model = Model(inputs=x, outputs=y)

    def build_conv(self,
                      input_shape,
                      nb_filters,
                      output_classes,
                      dilation_depth,
                      nb_stacks, 
                      use_skip_connections=True,
                      learn_all_outputs=True,
                      use_bias=True):
        # dilation_depth is how many layers inside a single block
        # nb_stacks is how many blocks in this model

        def residual_block(x):
            original_x = x
            tanh_out = Conv2D(nb_filters, 
                              2, 
                              strides=1,
                              dilation_rate=(2 ** i, 2 ** i),
                              # padding='valid',
                              padding='same',

                              use_bias=use_bias, 
                              # name='dilated_conv_%d_tanh_s%d' % (2 ** 1, s),
                              # name='dilated_conv_%d_tanh_s%d' % (2 ** 1, nb_stacks),
                              activation='tanh'
                              # kernel_constraint=max_norm(2.)
                              )(x)
        

            sigm_out = Conv2D(nb_filters,
                              2,
                              strides=1,
                              dilation_rate=(2 ** i, 2 ** i),
                              # dilation_rate=2 ** 1, 
                              # padding='valid',
                              padding='same',
                              use_bias=use_bias,
                              # name='dilated_conv_%d_sigm_s%d' % (2 ** 1, s),
                              # name='dilated_conv_%d_tanh_s%d' % (2 ** 1, nb_stacks),
                              activation='sigmoid'
                              # kernel_constraint=max_norm(2.)
                              )(x)
            x = multiply([tanh_out, sigm_out])

            res_x = Conv2D(nb_filters, 1, padding='same', use_bias=use_bias,kernel_constraint=max_norm(2.))(x)
            skip_x = Conv2D(nb_filters, 1, padding='same', use_bias=use_bias,kernel_constraint=max_norm(2.))(x)
            res_x = add([original_x, res_x])
            return res_x, skip_x
        
        print("In wavenet, input shape is ",input_shape)
        input = Input(shape=input_shape, name='input_layer')
        out = input
        skip_connections = []
        out = Conv2D(nb_filters, 2, dilation_rate=1, padding='same',name='initial_causal_conv')(out)
        
        print("build number of blocks/stacks: ", nb_stacks)
        print("build number of dilation layers within one block/stack: ", dilation_depth)

        for s in range(nb_stacks): # how many blocks
            for i in range(0, dilation_depth + 1):
                out, skip_out = residual_block(out)
                skip_connections.append(skip_out)
        
        out, skip_out = residual_block(out)
        skip_connections.append(skip_out)

        if use_skip_connections:
            out = add(skip_connections)

        out = Activation('relu')(out)
        out = Conv2D(input_shape[-1], 1, padding='same'
            # kernel_constraint=max_norm(2.)
            )(out)
        out = Activation('relu')(out)
        # out = Conv2D(input_shape[-1], 1, padding='same')(out) # original code
        out = Conv2D(1, 1, padding='same')(out)
        out = BatchNormalization()(out)

        if not learn_all_outputs:
            raise DeprecationWarning('Learning on just all outputs is wasteful, now learning only inside receptive field.')
            out = Lambda(lambda x: x[:, -1, :], output_shape=(out._keras_shape[-1],))(out)  # Based on gif in deepmind blog: take last output?

        out = Flatten()(out)
        # out = Activation('softmax', name="output_softmax")(out)

        out = Dense(1, 
            activation= 'linear',
            kernel_initializer='glorot_normal',
            use_bias= True,
            bias_initializer= 'glorot_normal')(out)

        print(input_shape, input_shape[-1])
        out = Dense(output_classes, activation='softmax')(out)

        return input, out



class MR_conv_kern(object):

    def __init__(self, **params):
        """
        MR_conv model.

            Args:
                input_shape (tuple of integers):
                    input dimension
                nb_filters (int):
                    how many filters in each layer              
                output_classes (int):
                    the depth of output
                dilation_depth (int):               
                nb_stacks (int):                
                use_skip_connections (bool):                
                learn_all_outputs (bool):           
                use_bias (bool):
        """

        model_params = {
            # 'input_shape': (params['range'],params['input_depth']),
            'input_shape': params['input_shape'],
            'nb_filters': params['nb_filters'],
            'output_classes': params['output_classes'],
            'dilation_depth': params['dilation_depth'],
            'nb_stacks': params['nb_stacks'],
            'use_skip_connections': True,
            'learn_all_outputs': True,
            'use_bias': True
        }

        x, y = self.build_conv(**model_params)
        
        self.model = Model(inputs=x, outputs=y)

    def build_conv(self,
                      input_shape,
                      nb_filters,
                      output_classes,
                      dilation_depth,
                      nb_stacks, 
                      use_skip_connections=True,
                      learn_all_outputs=True,
                      use_bias=True):
        # dilation_depth is how many layers inside a single block
        # nb_stacks is how many blocks in this model

        def residual_block(x):
            original_x = x
        
            tanh_out = Conv2D(nb_filters, 
                            3, 
                            strides=(2, 2), 
                            padding='same',
                            # dilation_rate=(1, 1), 
                            activation='tanh', 
                            use_bias=True, 
                            kernel_initializer='glorot_uniform')(x)

            sigm_out = Conv2D(nb_filters, 
                            3, 
                            strides=(2, 2), 
                            padding='same',
                            # dilation_rate=(1, 1), 
                            activation='sigmoid', 
                            use_bias=True, 
                            kernel_initializer='glorot_uniform')(x)

            x = multiply([tanh_out, sigm_out])

            res_x = Conv2D(nb_filters, 1, padding='same', use_bias=use_bias,kernel_constraint=max_norm(2.))(x)
            skip_x = Conv2D(nb_filters, 1, padding='same', use_bias=use_bias,kernel_constraint=max_norm(2.))(x)
            res_x = add([original_x, res_x])
            return res_x, skip_x
        
        print("In wavenet, input shape is ",input_shape)
        input = Input(shape=input_shape, name='input_layer')
        out = input
        skip_connections = []
        out = Conv2D(nb_filters, 2, dilation_rate=1, padding='valid',name='initial_causal_conv')(out)
        
        print("build number of blocks/stacks: ", nb_stacks)
        print("build number of dilation layers within one block/stack: ", dilation_depth)

        for s in range(nb_stacks): # how many blocks
            for i in range(0, dilation_depth + 1):
                out, skip_out = residual_block(out)
                skip_connections.append(skip_out)
        
        out, skip_out = residual_block(out)
        skip_connections.append(skip_out)

        if use_skip_connections:
            out = add(skip_connections)

        out = Activation('relu')(out)
        out = Conv2D(input_shape[-1], 1, padding='same'
            # kernel_constraint=max_norm(2.)
            )(out)
        out = Activation('relu')(out)
        # out = Conv2D(input_shape[-1], 1, padding='same')(out) # original code
        out = Conv2D(1, 1, padding='same')(out)
        out = BatchNormalization()(out)

        if not learn_all_outputs:
            raise DeprecationWarning('Learning on just all outputs is wasteful, now learning only inside receptive field.')
            out = Lambda(lambda x: x[:, -1, :], output_shape=(out._keras_shape[-1],))(out)  # Based on gif in deepmind blog: take last output?

        out = Flatten()(out)
        # out = Activation('softmax', name="output_softmax")(out)

        out = Dense(64, 
            activation= 'linear',
            kernel_initializer='glorot_normal',
            use_bias= True,
            bias_initializer= 'glorot_normal')(out)

        print(input_shape, input_shape[-1])
        out = Dense(output_classes, activation='softmax')(out)

        return input, out